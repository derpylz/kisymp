import matplotlib.pyplot as plt
import numpy as np


def plot_xor_dataset(X, y):
    """ Plot the data points of the XOR problem data sets. """
    plt.scatter(X[:, 0], X[:, 1], c=y.flatten(), cmap='PiYG', s=200)
    axes = plt.gca()
    axes.set_xlim([-1, 2])
    axes.set_ylim([-1, 2])
    plt.show()


def plot_xor_boundaries(X, y, model):
    """ Plot the decision boundaries of the XOR model. """
    h = 0.1
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

    # here "model" is your model's prediction (classification) function
    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap="PiYG", alpha=0.5)

    # Plot also the training points
    plt.scatter(X[:, 0], X[:, 1], c=y.flatten(), cmap="PiYG", s=200)
    plt.show()


def plot_mnist_dataset(x_train, y_train):
    """ Plot 9 examples of the the MNIST dataset. """
    plt.figure()
    for i in range(9):
        plt.subplot(3, 3, i+1)
        plt.tight_layout()
        plt.imshow(x_train[i].reshape([28, 28]),
                   cmap='gray', interpolation='none')
        plt.title("Digit: {}".format(np.where(y_train[i] == 1.0)[0][0]))
        plt.xticks([])
        plt.yticks([])

    plt.show()


def plot_mnist_prediction(x_test, y_test, model):
    """
    Plot 18 examples of the predictions of the MNIST model.
    """
    predicted_classes = model.predict_classes(x_test)
    test_classes = [np.where(x == 1.0)[0][0] for x in y_test]
    correct_indices = np.nonzero(predicted_classes == test_classes)[0]
    incorrect_indices = np.nonzero(predicted_classes != test_classes)[0]
    print("{} classes predicted incorrectly.".format(len(incorrect_indices)))

    # adapt figure size to accomodate 18 subplots
    plt.rcParams['figure.figsize'] = (8, 16)

    plt.figure()
    # plot 9 incorrect predictions
    for i, incorrect in enumerate(incorrect_indices[:9]):
        plt.subplot(6, 3, i+1)
        plt.imshow(x_test[incorrect].reshape([28, 28]),
                   cmap='gray', interpolation='none')
        plt.title(
            "Predicted {}, Truth: {}".format(predicted_classes[incorrect],
                                             np.where(y_test[incorrect] == 1.0)[0][0]))
        plt.xticks([])
        plt.yticks([])

    plt.show()
    
    print("{} classes predicted correctly.".format(len(correct_indices)))

    plt.figure()
    # plot 9 correct predictions
    for i, correct in enumerate(correct_indices[:9]):
        plt.subplot(6, 3, i+1)
        plt.imshow(x_test[correct].reshape([28, 28]),
                   cmap='gray', interpolation='none')
        plt.title(
            "Predicted: {}, Truth: {}".format(predicted_classes[correct],
                                              np.where(y_test[correct] == 1.0)[0][0]))
        plt.xticks([])
        plt.yticks([])

    plt.show()
